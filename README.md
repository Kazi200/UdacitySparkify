# Udacity Sparkify project
Udacity Data Scientist Capstone Project

You can find the blog post here:
https://medium.com/@kazi200/sparkify-an-exercise-in-predicting-churn-in-music-streaming-users-de3253735c08

## Project overview
With data replicated from a music streaming service similar to Spotify or Google Music, we are being tasked to analyse the data at hand using Spark, and then predict an outcome of whether a user would be likely to churn (that is, cancel or downgrade their service).

## Libraries used
 - pyspark.sql
 - pyspark.ml
 - sklearn.metrics
 - pandas
 
## Project motivation
The motivation in this project is solely my desire to acquaint myself with Spark. 
This project was a great foray into how Spark works, its benefits, and machine learning with the tool.   

## File and description
- Sparkify.ipynb - Jupyter notebook with all code used for this project 
- mini_sparkify_event_data.json - JSON file containing the mini dataset used for this project 
- ModelData_1.csv - Interim file with full set of features and scaled features. Used so as not to redo scaling which took some time

## Results 
Using the data provided, we were able to engineer a set of features that allowed us to build a predictive model that can predict whether a user will churn reasonable accuracy.

## Acknowledgements
- Udacity
- StackOverFlow
- Databricks
- Apache Spark 
